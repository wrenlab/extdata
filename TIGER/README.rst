TIGER - tissue-specific ESTs
============================

Quantification of tissue specificity for UniGenes.

Column descriptions in `format-description.gif`.

Links
=====
- http://bioinfo.wilmer.jhu.edu/tiger/
- https://www.ncbi.nlm.nih.gov/pubmed/16982645
